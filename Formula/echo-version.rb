class EchoVersion < Formula
  desc "Echo some hardcoded line"
  homepage "https://gitlab.com/antik9/echo-version"
  url "https://gitlab.com/antik9/echo-version/-/raw/0.1.25/echo-version"
  version "0.1.25"
  license "MIT"

  def install
    bin.install "echo-version"
  end

  test do
    system bin/"echo-version"
  end
end
